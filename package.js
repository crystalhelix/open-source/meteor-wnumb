Package.describe({
  name: 'crystalhelix:wnumb',
  version: '1.0.1',
  summary: 'wNumb formatter repackaged for Meteor',
  git: 'https://github.com/crystalhelix/meteor-wnumb',
  documentation: 'README.md'
});

Package.onUse(function(api) {
  api.versionsFrom('1.2.1');
  api.use('ecmascript');
  api.addFiles(['src/wNumb.js'], 'client');
});

Package.onTest(function(api) {
  api.use('ecmascript');
  api.use('tinytest');
  api.use('crystalhelix:wnumb');
  api.addFiles(['wnumb-tests.js'], 'client');
});
