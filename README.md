# wNumb for Meteor [![Build Status](https://travis-ci.org/crystalhelix/meteor-wnumb.svg)](https://travis-ci.org/crystalhelix/meteor-wnumb)

## Usage
This package makes the `wnumb` global variable available
on the client by default. For documentation, see the excellent examples [here](http://refreshless.com/wnumb/) or go directly to the noUiSlider repo [here](https://github.com/leongersen/wnumb/).

## Credits
All credits for this excellent javascript library go to the noUiSlider authors. This package is simply a wrapper to give you access to their awesome code.
